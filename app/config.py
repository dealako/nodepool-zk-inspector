"""
Common configuration routines.
"""
import argparse
import sys

import yaml

import log

DEFAULT_CONFIG = 'config.yaml'

# Setup custom logger
logger = log.setup_custom_logger('root')


def load_config():
    """
    Loads the configuration file.
    """

    # Parse any conf_file specification
    config_parser = argparse.ArgumentParser()

    config_parser.add_argument(
        "-c", "--conf-file",
        help="Specify config file",
        metavar="FILE")

    # Parse the configuration
    args, _ = config_parser.parse_known_args()

    config_file = DEFAULT_CONFIG

    if args.conf_file:
        config_file = args.conf_file

    try:
        with open(config_file, 'r') as ymlfile:
            cfg = yaml.safe_load(ymlfile)
        print("Loading configuration from file: " + config_file)
    except IOError:
        print(
            "Unable to read and load configuration from configuration file: " +
            config_file + ". Exiting...")
        sys.exit(1)

    return cfg


def load_config_file(config_file):
    """
    Loads the specified configuration file.
    """

    try:
        with open(config_file, 'r') as ymlfile:
            cfg = yaml.safe_load(ymlfile)
        print("Loading configuration from file: " + config_file)
    except IOError:
        print(
            "Unable to read and load configuration from configuration file: " +
            config_file + ". Exiting...")
        sys.exit(1)

    return cfg
