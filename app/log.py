"""
Common logger routines.
"""
import logging


def setup_custom_logger(name):
    """
    Sets up a custom logger using the specified name.

    :param name: the name of the logger
    :return: a custom logger using the specified name
    """

    # Returns a logger with the specified name, creating it if necessary.
    logger = logging.getLogger(name)

    # Setup the log handler, if necessary
    if not logger.handlers:
        formatter = logging.Formatter(fmt='[%(asctime)s][%(levelname)s] - %(message)s', datefmt='%Y-%m-%dT%H:%M:%S%z')
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(logging.DEBUG)

    return logger
