#!/usr/bin/env python3
"""
Routine to show and optionally delete empty Zookeeper NodePool nodes.
"""
from kazoo.client import KazooClient
import logging
import sys
import json
import click
import datetime
from datetime import timezone
import config
import log

# Setup custom logger
logger = log.setup_custom_logger('root')


@click.command(context_settings={'help_option_names': ['-h', '--help']})
@click.option('-n', '--nodes-path', default='/nodepool/nodes', is_flag=False, show_default=True, type=click.STRING,
              help="The nodepool node path")
@click.option('-d', '--delete-empty', default=False, is_flag=True, help="Delete the node if empty")
@click.option('-v', '--verbose', default=False, is_flag=True, help="Will print verbose messages")
def main(nodes_path, delete_empty, verbose):
    """
    Show Zookeeper NodePool node object details - optionally delete empty nodes
    """

    if verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    conf = config.load_config()
    zk_conf = conf['zookeeper']
    logger.debug("Zookeeper hosts: {}".format(zk_conf.get('hosts')))

    zk = KazooClient(hosts=zk_conf.get('hosts'))
    zk.start()

    # Counters
    total_count = 0
    empty_count = 0

    # Print the version of a node and its data
    data, stat = zk.get(nodes_path)
    logger.debug("Version: {}, data: {}".format(stat.version, data.decode("utf-8")))

    # List the children
    nodes = zk.get_children(nodes_path)
    logger.info("There are {} nodes with names {}".format(len(nodes), nodes))

    for node in nodes:
        logger.debug("Processing node {}".format(node))
        total_count += 1
        full_node_path = "{}/{}".format(nodes_path, node)
        data, stat = zk.get(full_node_path)

        # Convert the milliseconds since epoch to a string format
        create_time_str = datetime.datetime.fromtimestamp(stat.ctime / 1000.0, tz=timezone.utc).strftime(
            '%Y-%m-%dT%H:%M:%S%z')

        if not data:
            logger.info("Node {} is empty.".format(node))
            empty_count += 1

            # Should we delete the empty node?
            if delete_empty:
                logger.info("Deleting empty node: {}".format(full_node_path))
                zk.delete(full_node_path, recursive=True)
        else:
            logger.debug("Node: {}, created: {}, data: {}, stat: {}".
                         format(stat.version, create_time_str, data.decode("utf-8"), stat))

            # Convert JSON data to a dictionary
            json_data = json.loads(data.decode("utf-8"))

            logger.info("Node: {}, created: {}, host: {}, type: {}, region: {}, state: {}".
                        format(node, create_time_str, json_data['hostname'], json_data['type'], json_data['region'],
                               json_data['state']))

    zk.stop()

    # Print summary
    logger.info("Total nodes: {} with {} empty".format(total_count, empty_count))


if __name__ == "__main__":
    sys.exit(main())
