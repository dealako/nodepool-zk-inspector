# NodePool Zookeeper Inspector

[![pipeline status](https://gitlab.com/dealako/nodepool-zk-inspector/badges/master/pipeline.svg)](https://gitlab.com/dealako/nodepool-zk-inspector/commits/master)

A small collection of utilities to inspect and cleanup NodePool's Zookeeper instance.

## Setup

### Create and Load the VirtualEnv

```bash
virtualenv --python=/usr/local/bin/python3 .venv
source .venv/bin/activate
```

### Install Dependencies

```bash
pip install -r requirements.txt
pip install -r requirements-test.txt
```

## Configuration

Configuration is done via the `config.yaml` file. Currently we only have one property we leverage.

```bash
cat config.yaml
############################################################
# Configuration file
############################################################

zookeeper:
  hosts: localhost:2181
```

## Running

### Show NodePool Nodes

This utility reads the `/nodepool/nodes` path looking for nodes.  It iterates over the list of nodes and prints 
a few relevant bits of information.  The optional `--delete-empty` flag will delete nodes that have no data.

```bash
cd apps
# Usage with options
./show-nodepool-request-type.py -h
Usage: show-nodepool-request-type.py [OPTIONS]

  Show Zookeeper NodePool node object details - optionally delete empty
  nodes

Options:
  -n, --nodes-path TEXT  The nodepool node path  [default: /nodepool/nodes]
  -d, --delete-empty     Delete the node if empty
  -v, --verbose          Will print verbose messages
  -h, --help             Show this message and exit

# Example
./show-nodepool-nodes.py -v --delete-empty
```

Typical output may look like:

```code
./show-nodepool-request-type.py
Loading configuration from file: config.yaml
[2018-08-29T18:27:45+0000][INFO] - There are 36 nodes with names ['0000094293', '0000094295', '0000094296', '0000041040', '0000094332', '0000094331', '0000094334', '0000094333', '0000094336', '0000094335', '0000094338', '0000094337', '0000094339', '0000040624', '0000094284', '0000094285', '0000094321', '0000094320', '0000094323', '0000094322', '0000094325', '0000094324', '0000094327', '0000094326', '0000094329', '0000094328', '0000094270', '0000094277', '0000094313', '0000094318', '0000094280', '0000094263', '0000094266', '0000094268', '0000093057', '0000094302']
[2018-08-29T18:27:45+0000][INFO] - Node: 0000094293, created: 2018-08-29T16:30:20+0000, host: nodepool-ubuntu-xenial-om-io2-pubcloud-dfw-0000094293, type: ['ubuntu-xenial-om-io2'], region: DFW, state: in-use
[2018-08-29T18:27:45+0000][INFO] - Node: 0000094295, created: 2018-08-29T16:30:41+0000, host: nodepool-ubuntu-xenial-om-io2-pubcloud-dfw-0000094295, type: ['ubuntu-xenial-om-io2'], region: DFW, state: in-use
[2018-08-29T18:27:45+0000][INFO] - Node: 0000094296, created: 2018-08-29T16:31:15+0000, host: nodepool-ubuntu-xenial-om-io2-pubcloud-dfw-0000094296, type: ['ubuntu-xenial-om-io2'], region: DFW, state: in-use
[2018-08-29T18:27:45+0000][INFO] - Node: 0000041040, created: 2018-08-01T18:09:31+0000, host: nodepool-ubuntu-xenial-g1-8-pubcloud-ord-0000041040, type: ubuntu-xenial-g1-8, region: ORD, state: hold
[2018-08-29T18:27:45+0000][INFO] - Node: 0000094332, created: 2018-08-29T18:21:28+0000, host: nodepool-ubuntu-xenial-p2-15-pubcloud-ord-0000094332, type: ['ubuntu-xenial-p2-15'], region: ORD, state: ready
```

